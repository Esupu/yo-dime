package com.eshael.yodime.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by raphael on 11/1/16.
 */

public class ApiClient {

        public static final String BASE_URL = "http://178.79.143.149:8080/";
//        public static final String BASE_URL = "http://192.168.137.199:8080/video/";
        private static Retrofit retrofit = null;


    public static Retrofit getClient() {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        if (retrofit==null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();
            }
            return retrofit;
        }
}
