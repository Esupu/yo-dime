package com.eshael.yodime.util;

import com.eshael.yodime.model.ApiRequest;
import com.eshael.yodime.model.Response;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by raphael on 11/1/16.
 */

public interface ApiInterface
{
    @POST("YoDimeMobile/JsonUrl")
    Call<List<Response>> getContent(@Body ApiRequest apiRequest);
}
