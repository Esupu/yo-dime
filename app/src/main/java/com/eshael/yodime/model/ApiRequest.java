package com.eshael.yodime.model;

/**
 * Created by raphael on 11/1/16.
 */

public class ApiRequest {

    private String username;
    private String password;
    private String api_method;

    public ApiRequest() {
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setApi_method(String api_method) {
        this.api_method = api_method;
    }
}
