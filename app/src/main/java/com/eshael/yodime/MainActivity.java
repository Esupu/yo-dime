package com.eshael.yodime;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.eshael.yodime.adapter.MainAdapter;
import com.eshael.yodime.model.ApiRequest;
import com.eshael.yodime.model.Response;
import com.eshael.yodime.util.ApiClient;
import com.eshael.yodime.util.ApiInterface;
import com.eshael.yodime.util.ItemClickSupport;
import com.orm.SugarContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by raphael on 11/1/16.
 */

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private ProgressDialog progressDialog;
    private List<Response> responseList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SugarContext.init(this);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.main_recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh_view);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressDialog = new ProgressDialog(this);

        loadContent();

        ItemClickSupport.addTo(recyclerView)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v)
                    {
                        alerterSuccessSimple("",responseList.get(position).getUsername(),responseList.get(position).getEmail());
                    }
                });
    }

    private void loadContent()
    {
        List<Response> db = Response.listAll(Response.class);
        if(db.size()>1)
        {
            responseList = db;
            recyclerView.setAdapter(new MainAdapter(responseList, R.layout.main_row, this));
        }else
        {
            if (haveNetworkConnection())
            {
                getContent(false);
            }
            else
            {
                swipeRefreshLayout.setRefreshing(true);
            }
        }
    }

    private void getContent(boolean isRefreshing)
    {
        if(isRefreshing)
        {

            swipeRefreshLayout.setRefreshing(true);
        }else
        {
            progressDialog.setTitle("Load content");
            progressDialog.setMessage("Loading content...");
            progressDialog.show();
            progressDialog.setCancelable(false);
        }


        ApiInterface apiService =  ApiClient.getClient().create(ApiInterface.class);
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setApi_method("getinfo");
        apiRequest.setPassword("1234");
        apiRequest.setUsername("yodime");

        Call<List<Response>> call = apiService.getContent(apiRequest);
        call.enqueue(new Callback<List<Response>>() {
            @Override
            public void onResponse(Call<List<Response>> call, retrofit2.Response<List<Response>> response)
            {
                responseList = response.body();
                progressDialog.cancel();

                if (Response.listAll(Response.class).size()>1)
                {
                    Response.deleteAll(Response.class);
                }
                for(Response _response: responseList)
                {
                    _response.save();
                }

                recyclerView.setAdapter(new MainAdapter(responseList, R.layout.main_row, MainActivity.this));
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Response>>call, Throwable t) {
                progressDialog.cancel();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        getContent(true);
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public void alerterSuccessSimple(String header, String username,String password) {

        final String header2 = header;

        LayoutInflater li = LayoutInflater.from(MainActivity.this);
        View promptsView;
        promptsView = li.inflate(R.layout.qr_alert_dialog, null);

        TextView card_category = (TextView)promptsView.findViewById(R.id._username);
        TextView card_amount = (TextView)promptsView.findViewById(R.id.email);
        Button add_qr_card = (Button)promptsView.findViewById(R.id.details_okay);

        card_category.setText("Username :  " + username);
        card_amount.setText("Password :  " + password);

        add_qr_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            alert.cancel();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Details");
        builder.setView(promptsView);

        alert = builder.create();
        alert.setCancelable(false);
        alert.show();

    }
}
